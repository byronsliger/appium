package co.com.ceiba.appium.android;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.testdroid.appium.BaseTest;

import co.com.ceiba.appium.android.screens.ContactoScreen;
import co.com.ceiba.appium.android.screens.HomeContactoScreen;
import io.appium.java_client.android.AndroidDriver;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContactosAppiumTest extends BaseTest {

//    private static final String TARGET_APP_PATH = "BitbarSampleApp.apk";
    private static final String TESTDROID_SERVER = "http://127.0.0.1:4723";
	private HomeContactoScreen home;

    @BeforeClass
    public static void setUp() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.4.13");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion", "5.1.0");
		capabilities.setCapability("deviceName", "Android Emulator");
		capabilities.setCapability("androidPackage", "com.android.contacts");
		capabilities.setCapability("androidActivity", ".activities.PeopleActivity");
//		capabilities.setCapability("app", new File(ClassLoader.getSystemResource(TARGET_APP_PATH)
//                .getFile()).getAbsolutePath());
		capabilities.setCapability("unicodeKeyboard", true);
        
        System.out.println("Capabilities:" + capabilities.toString());

        System.out.println("Creating Appium session, this may take couple minutes..");
        driver = new AndroidDriver(new URL(TESTDROID_SERVER+"/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);

    }
    
    @AfterClass
    public static void tearDown()
    {
        if (driver != null) {
            driver.quit();
        }
    }


    @Test
    public void testAgregarContacto() throws IOException, InterruptedException {
    	home = new HomeContactoScreen(driver);
    	home.crearContacto();
    	new ContactoScreen(driver).registrarInformacionContacto("Byron Romero", "3003600799", "Work");
    	Assert.assertTrue(driver.findElement(By.name("Byron Romero")).isDisplayed());
    	driver.navigate().back();
    }
    
    @Test
    public void testEditarContacto() throws IOException, InterruptedException {
    	home = new HomeContactoScreen(driver);
    	home.seleccionarContacto("Byron Romero");
    	home.editarContacto();
    	new ContactoScreen(driver).modificarInformacionContacto("brs");
    	Thread.sleep(2000);
    	driver.swipe(411, 1156, 411, 910, 1000);
    	Assert.assertTrue(driver.findElement(By.xpath(".//*[@text='brs']")).isDisplayed());
    	driver.navigate().back();
    }
    
    @Test
    public void testEliminarContacto() throws IOException, InterruptedException {
    	home = new HomeContactoScreen(driver);
    	home.seleccionarContacto("Byron Romero");
    	home.eliminarContacto();
    }

}

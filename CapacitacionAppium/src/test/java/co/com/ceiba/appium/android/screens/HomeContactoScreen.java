package co.com.ceiba.appium.android.screens;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomeContactoScreen extends AbstractScreen {
	private static final String TEXT_OK = ".//*[@text='OK']";
	private static final String TEXT_DELETE = ".//*[@text='Delete']";
	private static final String MORE_OPTIONS = "More options";
	private static final String ADD_CONTACT = "com.android.contacts:id/floating_action_button_container";
	private static final String EDIT_CONTACT = "com.android.contacts:id/menu_edit";

	public HomeContactoScreen(AppiumDriver driver) {
		super(driver);
	}
	
	@AndroidFindBy(id=ADD_CONTACT)
	WebElement addContact;
	@AndroidFindBy(id=EDIT_CONTACT)
	WebElement editContact;
	@AndroidFindBy(name=MORE_OPTIONS)
	WebElement moreOptions;
	@AndroidFindBy(xpath=TEXT_DELETE)
	WebElement deleteText;
	@AndroidFindBy(xpath=TEXT_OK)
	WebElement okText;
	
	public void crearContacto () {
		addContact.click();
	}
	public void editarContacto () {
		editContact.click();
	}
	
	public void seleccionarContacto(String nombreContacto) throws InterruptedException{
		Thread.sleep(2000);
		List<WebElement> contactosElementos = driver.findElements(By.xpath(".//*[@text='" + nombreContacto + "']"));
		if (!contactosElementos.isEmpty()) {
			contactosElementos.get(0).click();;
		}
	}
	
	public void eliminarContacto(){
		moreOptions.click();
		deleteText.click();
		okText.click();
	}

}

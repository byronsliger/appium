package co.com.ceiba.appium.android.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ContactoScreen extends AbstractScreen {

	private static final String TEXT_MOBILE = ".//*[@text='Mobile']";
	private static final String TEXT_PHONE = ".//*[@text='Phone']";
	private static final String TEXT_NAME = ".//*[@text='Name']";
	private static final String TEXT_NICKNAME = ".//*[@text='Nickname']";
	
	@AndroidFindBy(xpath=TEXT_NAME)
	WebElement textName;
	@AndroidFindBy(xpath=TEXT_PHONE)
	WebElement textPhone;
	@AndroidFindBy(xpath=TEXT_MOBILE)
	WebElement textMobile;
	@AndroidFindBy(xpath=TEXT_NICKNAME)
	WebElement textNickmare;
	

	public ContactoScreen(AppiumDriver driver) {
		super(driver);
	}

	public void registrarInformacionContacto(String nombreContacto, String telefonoContacto, String tipoTelefono) {
		textName.sendKeys(nombreContacto);
		textPhone.sendKeys(telefonoContacto);
		textMobile.click();
		driver.findElement(By.xpath(this.getXpathByText(tipoTelefono))).click();
		driver.navigate().back();
	}
	
	public void modificarInformacionContacto (String nickmare) {
		textNickmare.sendKeys(nickmare);
		driver.navigate().back();
	}
	
}

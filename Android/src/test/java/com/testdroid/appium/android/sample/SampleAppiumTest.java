package com.testdroid.appium.android.sample;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.testdroid.appium.BaseTest;

import io.appium.java_client.android.AndroidDriver;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SampleAppiumTest  extends BaseTest {

    private static final String TARGET_APP_PATH = "BitbarSampleApp.apk";
    private static final String TESTDROID_SERVER = "http://127.0.0.1:4723";

    @BeforeClass
    public static void setUp() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.4.16");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion", "2.3.3");
		capabilities.setCapability("deviceName", "SM-P555M");
		capabilities.setCapability("app", new File(ClassLoader.getSystemResource(TARGET_APP_PATH)
                .getFile()).getAbsolutePath());
		capabilities.setCapability("unicodeKeyboard", true);
        
        System.out.println("Capabilities:" + capabilities.toString());

        System.out.println("Creating Appium session, this may take couple minutes..");
        wd = new AndroidDriver(new URL(TESTDROID_SERVER+"/wd/hub"), capabilities);
    }
    @AfterClass
    public static void tearDown()
    {
        if (wd != null) {
            wd.quit();
        }
    }


    @Test
    public void mainPageTest() throws IOException, InterruptedException {

        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        takeScreenshot("start");
        wd.findElement(By.xpath("//android.widget.RadioButton[@text='Use Testdroid Cloud']")).click();
        wd.findElement(By.xpath("//android.widget.EditText[@resource-id='com.bitbar.testdroid:id/editText1']")).sendKeys("John Doe");
        takeScreenshot("after_adding_name");
        //wd.navigate().back();
        wd.findElement(By.xpath("//android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.Button[1]")).click();
        takeScreenshot("after_answer");
        //ad.pressKeyCode(AndroidKeyCode.KEYCODE_CAMERA);
    }

}

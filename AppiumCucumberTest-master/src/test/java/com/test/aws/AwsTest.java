package com.test.aws;

import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.test.apidemo.app.screens.AppScreen;
import com.test.apidemo.app.screens.HomeScreen;
import com.test.utils.AppUtils;

import io.appium.java_client.MobileBy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/cucumber.xml"})
public class AwsTest {
    @Autowired
    private AppUtils utils;
    
    @Autowired
    public HomeScreen homeScreen;
    @Autowired
    public AppScreen appScreen;
    
    @Test
    public void viewActivityScreen(){
    	appScreen = homeScreen.getAppScreen();
    	Assert.assertTrue(appScreen.isElementPresent(MobileBy.AccessibilityId("Activity")));
    	utils.takeScreenshot("Prueba_Activity");
    }
    
    @After
    public void clearSession(){
        try {
            utils.getDriver().quit();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}

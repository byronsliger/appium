package com.testdroid.appium.ios.sample;

import io.appium.java_client.ios.IOSDriver;
import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.testdroid.appium.BaseTest;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SampleAppiumiOSTest extends BaseTest {
    protected static final String TESTDROID_SERVER = "http://127.0.0.1:4723";
    protected static final String TARGET_APP_PATH = "/Users/bairon.romero/Work/Funcionales/Appium/IOS/app/Prueba.app";
	private static DesiredCapabilities capabilities;

    @BeforeClass
    public static void setUp() throws Exception {

        capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone 6");
        capabilities.setCapability("appium-version", "1.4.13");
		capabilities.setCapability("platformVersion", "9.3");
		capabilities.setCapability("app", TARGET_APP_PATH);
		capabilities.setCapability("noReset", true);
//		capabilities.setCapability("browserName", "Safari"); 

        //capabilities.setCapability("app", "settings");
        System.out.println("Capabilities:" + capabilities.toString());
        wd = new IOSDriver(new URL(TESTDROID_SERVER+"/wd/hub"), capabilities);
    }
    
    public static void resetDriver() throws MalformedURLException
    {
        if(wd != null) {
            wd.quit();
            wd = new IOSDriver(new URL(TESTDROID_SERVER+"/wd/hub"), capabilities);
        }

    }

    @Test
    public void mainPageTest() throws Exception {
    	String name = "Ceiba Movil";
        try {
        	takeScreenshot("begin.png");
        	wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    		wd.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[1]")).sendKeys(name);
    		wd.findElement(By.name("Ver nombre")).click();
    		WebElement lblResult = wd.findElement(By.name(name));
    		assertEquals(name, lblResult.getText());
    		takeScreenshot("Success.png");
        } catch( Exception e ) {
            takeScreenshot("Failed.png");
            throw e;
        }
        resetDriver();

    }

    @Test
    public void mainPageTest2() throws Exception {
    	String name = "BRS";
    	try {
    		takeScreenshot("begin.png");
    		wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    		wd.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[1]")).sendKeys(name);
    		wd.findElement(By.name("Ver nombre")).click();
    		WebElement lblResult = wd.findElement(By.name(name));
    		assertEquals(name, lblResult.getText());
    		takeScreenshot("Success.png");
    	} catch( Exception e ) {
    		takeScreenshot("Failed.png");
    		throw e;
    	}
    	resetDriver();
    }
    

}
